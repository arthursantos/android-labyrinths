package br.ufrgs.inf.labyrinths;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Debug;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class MainActivity extends AppCompatActivity {
    private final Semaphore semaphore = new Semaphore(0, true);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MainActivity that = this;

        this.checkPermissions();

        final Button nextButton = (Button)findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                semaphore.release();
            }
        });

        ThreadGroup group = new ThreadGroup("threadGroup");
        Thread th = new Thread(group, new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        that.setupButton("Start", true);
                        semaphore.acquire();

                        int size = Integer.parseInt(((EditText) findViewById(R.id.sizeTextEdit)).getText().toString());
                        that.setupButton("Start", false);

                        //Generates and shows some mazes
                        ArrayList<Maze> mazes = new ArrayList<>();
                        for (int i = 0; i < 5; i ++){
                            Maze maze = new Maze(size, size);
                            mazes.add(maze);
                            that.printMaze(maze);
                            Thread.sleep(1000);
                        }


                        //Wall Follower
                        that.setupButton("Resolve Wall Follower", true);
                        semaphore.acquire();
                        that.setupButton("Resolve Wall Follower", false);

                        //Debug.startMethodTracing("wallfollower" + size);
                        long time = System.currentTimeMillis();
                        for(Maze maze: mazes){
                            maze.solveWallFollower();
                        }
                        that.setTime((float)(System.currentTimeMillis() - time));
                        //Debug.stopMethodTracing();

                        //Shows the solutions
                        for(Maze maze: mazes) {
                            that.printMaze(maze);
                            Thread.sleep(1000);
                        }


                        //Recursive
                        that.setupButton("Resolve Recursive", true);
                        semaphore.acquire();
                        that.setupButton("Resolve Recursive", false);

                        //Debug.startMethodTracing("recursive" + size);
                        time = System.currentTimeMillis();
                        for(Maze maze: mazes){
                            maze.solveRecursivelly();
                        }
                        that.setTime((float)(System.currentTimeMillis() - time));
                        //Debug.stopMethodTracing();

                        //Shows the solutions
                        for(Maze maze: mazes) {
                            that.printMaze(maze);
                            Thread.sleep(1000);
                        }

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, "myThread", 1000000);
        th.start();

    }

    private void checkPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        0);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private void setupButton(final String text, final boolean enabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button nextButton = (Button)findViewById(R.id.nextButton);
                nextButton.setText(text);
                nextButton.setEnabled(enabled);
            }}
        );
    }

    public void printMaze(final Maze maze) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView)findViewById(R.id.txt_labyrinth);
                TextView textViewPath = (TextView)findViewById(R.id.txt_path);

                textView.setText(maze.toString());
                textViewPath.setText(maze.pathToString());
            }
        });
    }

    public void setTime(final float time) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView)findViewById(R.id.timeTextView);
                textView.setText(String.valueOf(time) + " ms");
            }
        });
    }

}
