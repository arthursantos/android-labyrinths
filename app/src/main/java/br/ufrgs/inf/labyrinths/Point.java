package br.ufrgs.inf.labyrinths;

/**
 * Created by arthursantos on 11/1/16.
 */

public class Point {
    public int x;
    public int y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        return Point.class.isInstance(obj) && this.x == ((Point)obj).x && this.y == ((Point)obj).y;
    }
}
